export const host = 'https://www.nannyhr.cn'

export function getRequestUrl(url: string) {
    return `${host}${url}`
}
