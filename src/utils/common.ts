export function getDifferentKeys(obj1, obj2) {
    const differentKeys = [];

    for (const key in obj1) {
        if (obj1.hasOwnProperty(key)) {
            if (!obj2.hasOwnProperty(key) || !isEqual(obj1[key], obj2[key])) {
                differentKeys.push(key);
            }
        }
    }

    for (const key in obj2) {
        if (obj2.hasOwnProperty(key) && !obj1.hasOwnProperty(key)) {
            differentKeys.push(key);
        }
    }

    return differentKeys;
}

// 检查是否相等的辅助函数
function isEqual(value1, value2) {
    // 如果值为 null 或 undefined，则直接比较
    if ((value1 == null) && (value2 == null)) {
        return true;
    }
    // 如果值为数组，则使用深度比较
    if (Array.isArray(value1) && Array.isArray(value2)) {
        return arraysEqual(value1, value2);
    }
    // 其他情况下直接比较
    return value1 === value2;
}

// 检查数组是否相等的辅助函数
function arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length) {
        return false;
    }
    for (let i = 0; i < arr1.length; i++) {
        if (!isEqual(arr1[i], arr2[i])) {
            return false;
        }
    }
    return true;
}
