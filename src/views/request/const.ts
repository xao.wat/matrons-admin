import { NeedStatus } from "@/api/request";

export const needConf = {
  [NeedStatus.receivedOrder]: '已接单',
  [NeedStatus.published]: '已发布',
  [NeedStatus.notPublish]: '未发布',
}