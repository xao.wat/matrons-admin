import { OrderStatus, DepositStatus } from "@/api/order";

export const orderConf = {
  [OrderStatus.notSigned]: '待签约',
  [OrderStatus.notHousing]: '待上户',
  [OrderStatus.onHousing]: '已上户',
  [OrderStatus.finished]: '已完成',
  [OrderStatus.cancel]: '已取消',
  [OrderStatus.audit]: '待审核',
}

export const depositConf = {
  [DepositStatus.not]: '未支付',
  [DepositStatus.success]: '支付成功',
  [DepositStatus.fail]: '支付失败',
  [DepositStatus.return]: '定价已退款'
}