import request from '@/config/axios'

export enum NeedStatus {
  receivedOrder = 0x2,
  published = 0x1,
  notPublish = 0x0,
}

export interface RequestVO {
  id: number
  userId: number
  reqStartDate: Date
  reqEndDate: Date
  upSalary: number
  downSalary: number
  allowConnect: boolean
  isSingleBaby: boolean
  workArea: string
  remark: string
  status: NeedStatus
  isRushReq: boolean
  reqCycle: number
  areaId: number
}

// 查询需求列表
export const getRequestPage = async (params) => {
  return await request.get({ url: `/yuesao/request/page`, params })
}

// 查询需求详情
export const getRequest = async (id: number) => {
  return await request.get({ url: `/yuesao/request/get?id=` + id })
}

// 新增需求
export const createRequest = async (data: RequestVO) => {
  return await request.post({ url: `/yuesao/request/create`, data })
}

// 修改需求
export const updateRequest = async (data: Partial<RequestVO>) => {
  return await request.post({ url: `/yuesao/request/update`, data })
}

// 删除需求
export const deleteRequest = async (id: number) => {
  return await request.delete({ url: `/yuesao/request/delete?id=` + id })
}

// 导出需求 Excel
export const exportRequest = async (params) => {
  return await request.download({ url: `/yuesao/request/export-excel`, params })
}

export const setRushRequest = async (data: { id: number }) => {
  return await request.post({ url: `/yuesao/request/update_is_rush`, data });
}
