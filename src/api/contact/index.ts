import request from '@/config/axios'

export interface IContact {
  userId: number;
  yuesaoId: number;
  createTime: number;
}

// 查询需求列表
export const getRequestPage = async (params) => {
  return await request.get({ url: `/yuesao/first-connect/page`, params })
}