import request from '@/config/axios'

export interface SuggestionVO {
  id: number
  pushId: number
  targetId: number
  userType: number
  comment: string
  suggestionType: number
  pushType: 100 | 101
}

// 查询留言反馈列表
export const getSuggestionPage = async (params) => {
  return await request.get({ url: `/yuesao/suggestion/page`, params })
}

// 查询留言反馈详情
export const getSuggestion = async (id: number) => {
  return await request.get({ url: `/yuesao/suggestion/get?id=` + id })
}

// 新增留言反馈
export const createSuggestion = async (data: SuggestionVO) => {
  return await request.post({ url: `/yuesao/suggestion/create`, data })
}

// 修改留言反馈
export const updateSuggestion = async (data: SuggestionVO) => {
  return await request.put({ url: `/yuesao/suggestion/update`, data })
}

// 删除留言反馈
export const deleteSuggestion = async (id: number) => {
  return await request.delete({ url: `/yuesao/suggestion/delete?id=` + id })
}

// 导出留言反馈 Excel
export const exportSuggestion = async (params) => {
  return await request.download({ url: `/yuesao/suggestion/export-excel`, params })
}

// 处理此意见反馈
export const updateFeedback = async (params: { id: number; state: number; pushId: number, pushType: number }) => {
  return await request.get({ url: `/yuesao/suggestion/update_is_handle`, params })
}
