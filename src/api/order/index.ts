import request from '@/config/axios'

export enum OrderStatus {
  notSigned = 0x0,
  notHousing = 0x1,
  onHousing = 0x2,
  finished = 0x3,
  cancel = 0x4,
  audit = 0x5,
}

export enum DepositStatus {
  not = 0x0,
  success = 0x1,
  fail = 0x2,
  return = 0x3
}

export interface OrderVO {
  id: number
  inDate: number
  outDate: number
  price: number
  city: string
  contractId: number
  requestId: number
  yuesaoId: number
  userId: number
  status: OrderStatus
  dingjinStatus: DepositStatus
  dingjinMount: number
  comment: string
  supplement: string
  signTime: Date
  dingjinIsPlatform: boolean
  refundTime: Date
}

// 查询订单表	列表
export const getOrderPage = async (params) => {
  return await request.get({ url: `/yuesao/order/page`, params })
}

// 查询订单表	详情
export const getOrder = async (id: number) => {
  return await request.get({ url: `/yuesao/order/get?id=` + id })
}

// 新增订单表
export const createOrder = async (data: OrderVO) => {
  return await request.post({ url: `/yuesao/order/create`, data })
}

// 修改订单表
export const updateOrder = async (data: OrderVO) => {
  return await request.put({ url: `/yuesao/order/update`, data })
}

// 删除订单表
export const deleteOrder = async (id: number) => {
  return await request.delete({ url: `/yuesao/order/delete?id=` + id })
}

// 导出订单表	 Excel
export const exportOrder = async (params) => {
  return await request.download({ url: `/yuesao/order/export-excel`, params })
}

// 订单审核
export const audit = async (params) => {
  return await request.post({ url: `yuesao/order/update_order_state`, data: { order_id: params.orderId, state: params.state, comment:  params.comment }})
}

export const getContract = async (data: { id: number }) => {
  return await request.post({ url: `/yuesao/order/get_contract_url_by_id`, data });
}
