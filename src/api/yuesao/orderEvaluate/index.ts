import request from '@/config/axios'

export interface OrderEvaluateVO {
  id: number
  parentId: number
  yuesaoId: number
  orderId: number
  comment: string
  remarks: string
  point: number
  isShow: number
  picUrl: string
}

// 查询订单月嫂评价表	列表
export const getOrderEvaluatePage = async (params) => {
  return await request.post({ url: `/yuesao/order-evaluate/page`, data: params })
}

// 查询订单月嫂评价表	详情
export const getOrderEvaluate = async (id: number) => {
  return await request.get({ url: `/yuesao/order-evaluate/get?id=` + id })
}

// 新增订单月嫂评价表	
export const createOrderEvaluate = async (data: OrderEvaluateVO) => {
  return await request.post({ url: `/yuesao/order-evaluate/create`, data })
}

// 修改订单月嫂评价表	
export const updateOrderEvaluate = async (data: OrderEvaluateVO) => {
  return await request.put({ url: `/yuesao/order-evaluate/update`, data })
}

// 删除订单月嫂评价表	
export const deleteOrderEvaluate = async (id: number) => {
  return await request.delete({ url: `/yuesao/order-evaluate/delete?id=` + id })
}

// 导出订单月嫂评价表	 Excel
export const exportOrderEvaluate = async (params) => {
  return await request.download({ url: `/yuesao/order-evaluate/export-excel`, params })
}

// shenhe
export const auditEvaluate = async (data) => {
  return await request.post({ url: `/yuesao/order-evaluate/checkEvaluate`, data })
}
