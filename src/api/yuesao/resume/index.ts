import request from '@/config/axios'

export interface ResumeVO {
    id: number
    yuesaoId: number
    avatar: string
    wechat: string
    mobile: string
    schoolDegree: number
    workHistory: string
    workSkill: string
    expectedSalary: number
    age: number
    origin: string
    hasChildren: number
    workYear: number
    nation: string
    selfIntro: string
    selfRemarks: string
    isBaoxian: number
    isTijian: number
    baoxianUrl: string
    baoxianTime: Date
    tijianUrl: string
    tijianUrls: string[]
    tijianTime: Date
    isBeidiao: byte
    beidiaoTime: Date
    workUrls: string[]
    evaluateUrls: string[]
    yueZiCanUrls: string[]
}

// 查询月嫂简历列表
export const getResumePage = async (params) => {
    return await request.post({url: `/yuesao/yuesao/getYuesaoByPage`, data: params})
}
// 查询月嫂简历待审核列表
export const getCheckResumePage = async (params) => {
    return await request.post({url: `/yuesao/resume/getCheckResume`, data: params})
}

// 查询月嫂简历详情
export const getResume = async (id: number) => {
    return await request.post({url: `/yuesao/resume/getResumeByYuesao`, data: {id}})
}

// 新增月嫂简历
export const createResume = async (data: ResumeVO) => {
    return await request.post({url: `/yuesao/yuesao/create`, data})
}

// 修改月嫂简历
export const updateResume = async (data: ResumeVO) => {
    return await request.post({url: `/yuesao/resume/update`, data})
}

// 删除月嫂简历
export const deleteResume = async (id: number) => {
    return await request.post({url: `/yuesao/resume/delete`, data: {id}})
}
// 拉黑
export const bannedResume = async (id: number) => {
    return await request.post({url: `/yuesao/yuesao/banned`, data: {id}})
}
// 解封
export const cancelBannedResume = async (id: number) => {
    return await request.post({url: `/yuesao/yuesao/cancelBanned`, data: {id}})
}

// 导出月嫂简历 Excel
export const exportResume = async (params) => {
    return await request.download({url: `/yuesao/resume/export-excel`, params})
}

// 简历审核
export const audit = async (params) => {
    return await request.post({url: `/yuesao/resume/checkResume`, data: params})
}

// 简历审核差异
export const compare = async (params) => {
    return await request.post({url: `/yuesao/resume/getCheckResumeDetail`, data: params})
}


export const getArea = async () => {
    return await request.get({url: `/system/area/get-by-ip?ip=172.68.1.1`})
}


export interface IAreaItem {
    id: string;
    name: string;
    children: IAreaItem[];
}

/**
 * 获取省市区
 */
export const fetchAreaTree = async (): Promise<IAreaItem[]> => {
    return await request.get<IAreaItem[]>({url: '/system/area/tree'});
};

export interface IDicItem {
    label: string;
    value: string;
}

export interface IDicRes {
    yuesao_nation: IDicItem[]; // 民族
    yuesao_schoolDegree: IDicItem[]; //  学历
}

/**
 * 获取字典
 */
export const fetchDics = async (): Promise<IDicRes> => {
    return await request.get<IDicRes>({url: '/ys-common/dict/all'});
};

export interface IRemarkItem {
    id: string;
    remark: string;
}

/**
 * 获取标签
 */
export const fetchRemarks = async (): Promise<IRemarkItem[]> => {
    const data = await request.get<{
        list: IRemarkItem[]
    }>({url: '/yuesao/yuesaoremark/get_remark'});
    return data.list || []
};
