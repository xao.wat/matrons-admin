import request from '@/config/axios'

export interface ImPcVO {
  id: number
  receivedId: number
  content: string
  type: number
  isRead: byte
  receivedType: number
  fromDate: Date
  orderId: number
}

// 查询聊天消息列表
export const getImPcPage = async (params) => {
  return await request.get({ url: `/yuesao/im-pc/page`, params })
}

// 查询聊天消息详情
export const getImPc = async (id: number) => {
  return await request.get({ url: `/yuesao/im-pc/get?id=` + id })
}

// 新增聊天消息
export const createImPc = async (data: ImPcVO) => {
  return await request.post({ url: `/yuesao/im-pc/create`, data })
}

// 修改聊天消息
export const updateImPc = async (data: ImPcVO) => {
  return await request.put({ url: `/yuesao/im-pc/update`, data })
}

// 删除聊天消息
export const deleteImPc = async (id: number) => {
  return await request.delete({ url: `/yuesao/im-pc/delete?id=` + id })
}

// 导出聊天消息 Excel
export const exportImPc = async (params) => {
  return await request.download({ url: `/yuesao/im-pc/export-excel`, params })
}
