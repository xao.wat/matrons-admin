import request from '@/config/axios'

export interface YSUserVO {
  id: number
  nickname: string
  password: string
  status: number
  registerIp: string
  loginIp: string
  loginDate: Date
  avatar: string
  name: string
  sex: number
  mobile: string
  cityId: string
  cityName: string
  isVip: number
  vipExpireDate: Date
  isBanned: number
  point: number
  banTime: Date
  releaseTime: Date
}

// 查询月嫂程序用户列表
export const getYSUserPage = async (params) => {
  return await request.get({ url: `yuesao/ysuser/page`, params })
}

// 查询月嫂程序用户详情
export const getYSUser = async (id: number) => {
  return await request.post({ url: '/yuesao/ysuser/get', data: { id } })
}

// 新增月嫂程序用户
export const createYSUser = async (data: YSUserVO) => {
  return await request.post({ url: `/yuesao/YS-user/create`, data })
}

// 修改月嫂程序用户
export const updateYSUser = async (data: Partial<YSUserVO>) => {
  return await request.post({ url: `/yuesao/ysuser/update`, data })
}

// 删除月嫂程序用户
export const deleteYSUser = async (id: number) => {
  return await request.delete({ url: `/yuesao/ysuser/delete?id=` + id })
}

// 导出月嫂程序用户 Excel
export const exportYSUser = async (params) => {
  return await request.download({ url: `/yuesao/YS-user/export-excel`, params })
}

// 设置VIP
export const setVipYSUser = async (user_id: number) => {
  return await request.post({ url: `/yuesao/ysuser/change/vip/success`, data: { user_id } });
}

// 设置拉黑
export const setBannedYSUser = async (user_id: number) => {
  return await request.post({ url: `/yuesao/ysuser/change/banned`, data: { user_id } });
}